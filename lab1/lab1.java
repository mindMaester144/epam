package lab1;

public class lab1 {
    public static void main(String[] argv) {
        TypesTest.startTestCases();
    }
}

class TypesTest {
    public static void startTestCases() {
        testCaseByte();
        testCaseShort();
        testCaseInt();
        testCaseLong();
        testCaseChar();
        testCaseFloat();
        testCaseDouble();
        testCaseBool();
        testCaseString();
        testCaseObject();
    }


    public static void testCaseByte(){
        System.out.println("\n\n------Byte test------");

        byte variable1 = 10;
        byte variable2 = 20;
        int resultVariable;


        System.out.println("\n +, -, *, /, %:");

        resultVariable = variable1 + variable2;
        printOperation("+", variable1, variable2, resultVariable);

        resultVariable = variable1 - variable2;
        printOperation("-", variable1, variable2, resultVariable);

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);

        resultVariable = variable1 / variable2;
        printOperation("/", variable1, variable2, resultVariable);

        resultVariable = variable1 % variable2;
        printOperation("%", variable1, variable2, resultVariable);

        resultVariable = variable1 & variable2;
        printOperation("&", variable1, variable2, resultVariable);

        resultVariable = variable1 ^ variable2;
        printOperation("^", variable1, variable2, resultVariable);

        resultVariable = variable1 | variable2;
        printOperation("|", variable1, variable2, resultVariable);


        System.out.println("\nwith zero:");

        variable1 = 10;
        variable2 = 0;

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);


        try {
            resultVariable = variable1 / variable2;
            printOperation("/", variable1, variable2, resultVariable);
        }
        catch (ArithmeticException e) { ;
            printOperationError("/", variable1, variable2, e.getMessage());
        }

        try {
            resultVariable = variable1 % variable2;
            printOperation("%", variable1, variable2, resultVariable);
        }
        catch (ArithmeticException e) {
            printOperationError("%", variable1, variable2, e.getMessage());
        }

        variable1 = 0;
        variable2 = 10;

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);

        resultVariable = variable1 / variable2;
        printOperation("/", variable1, variable2, resultVariable);

        resultVariable = variable1 % variable2;
        printOperation("%", variable1, variable2, resultVariable);

        variable1 = 10;
        variable2 = 1;


        System.out.println("\n>>, >>>, <<:");

        resultVariable = variable1 >> variable2;
        printOperation(">>", variable1, variable2, resultVariable);

        resultVariable = variable1 >>> variable2;
        printOperation(">>>", variable1, variable2, resultVariable);

        resultVariable = variable1 << variable2;
        printOperation("<<", variable1, variable2, resultVariable);

        variable1 = -10;
        variable2 = 1;

        resultVariable = variable1 >> variable2;
        printOperation(">>", variable1, variable2, resultVariable);

        resultVariable = variable1 >>> variable2;
        printOperation(">>>", variable1, variable2, resultVariable);

        resultVariable = variable1 << variable2;
        printOperation("<<", variable1, variable2, resultVariable);


        System.out.println("\n--, ++:");

        variable1 = 5;
        variable2 = variable1;

        variable1++;
        System.out.printf("variable1++: before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        ++variable1;
        System.out.printf("++variable1: before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1--;
        System.out.printf("variable1--: before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        --variable1;
        System.out.printf("--variable1: before: %d, after: %d\n", variable2, variable1);


        System.out.printf("Print 'variable1++' (variable1 == %d): %d\n", variable1, variable1++);
        System.out.printf("Print '++variable1' (variable1 == %d): %d\n", variable1, ++variable1);
        System.out.printf("Print 'variable1--' (variable1 == %d) %d:\n", variable1, variable1--);
        System.out.printf("Print '--variable1' (variable1 == %d): %d\n", variable1, --variable1);


        System.out.println("+=, -=, *=, /=, %=");
        variable1 = 100;

        variable2 = variable1;
        variable1+=5;
        System.out.printf("variable1+=5: variable1 before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1-=5;
        System.out.printf("variable1-=5: variable1 before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1*=5;
        System.out.printf("variable1*=5: variable1 before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1/=5;
        System.out.printf("variable1/=5: variable1 before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1%=5;
        System.out.printf("variable1%%=5: variable1 before: %d, after: %d\n", variable2, variable1);


        System.out.println("Types");
        variable1 = 10;

        short shortVar = variable1;
        int intVar = variable1;
        long longVar = variable1;
        char charVar = (char)variable1;

        System.out.println("Byte " + variable1 + "to short:" + shortVar);
        System.out.println("Byte " + variable1 + " to int:" + intVar);
        System.out.println("Byte " + variable1 + " to long:" + longVar);
        System.out.println("Byte " + variable1 + " to char:" + charVar);

        System.out.println("Relational:");

        variable1 = 100;
        variable2 = 101;

        boolean boolResult = variable1 == variable2;
        printOperation("==", variable1, variable2, boolResult);

        boolResult = variable1 != variable2;
        printOperation("!=", variable1, variable2, boolResult);

        boolResult = variable1 >= variable2;
        printOperation(">=", variable1, variable2, boolResult);

        boolResult = variable1 <= variable2;
        printOperation("<=", variable1, variable2, boolResult);

        boolResult = variable1 > variable2;
        printOperation(">", variable1, variable2, boolResult);

        boolResult = variable1 < variable2;
        printOperation("<", variable1, variable2, boolResult);

    }

    public static void testCaseShort(){
        System.out.println("\n\n------Short test------");

        short variable1 = 10;
        short variable2 = 20;
        int resultVariable;


        System.out.println("\n +, -, *, /, %:");

        resultVariable = variable1 + variable2;
        printOperation("+", variable1, variable2, resultVariable);

        resultVariable = variable1 - variable2;
        printOperation("-", variable1, variable2, resultVariable);

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);

        resultVariable = variable1 / variable2;
        printOperation("/", variable1, variable2, resultVariable);

        resultVariable = variable1 % variable2;
        printOperation("%", variable1, variable2, resultVariable);

        resultVariable = variable1 & variable2;
        printOperation("&", variable1, variable2, resultVariable);

        resultVariable = variable1 ^ variable2;
        printOperation("^", variable1, variable2, resultVariable);

        resultVariable = variable1 | variable2;
        printOperation("|", variable1, variable2, resultVariable);


        System.out.println("\nwith zero:");

        variable1 = 10;
        variable2 = 0;

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);


        try {
            resultVariable = variable1 / variable2;
            printOperation("/", variable1, variable2, resultVariable);
        }
        catch (ArithmeticException e) { ;
            printOperationError("/", variable1, variable2, e.getMessage());
        }

        try {
            resultVariable = variable1 % variable2;
            printOperation("%", variable1, variable2, resultVariable);
        }
        catch (ArithmeticException e) {
            printOperationError("%", variable1, variable2, e.getMessage());
        }

        variable1 = 0;
        variable2 = 10;

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);

        resultVariable = variable1 / variable2;
        printOperation("/", variable1, variable2, resultVariable);

        resultVariable = variable1 % variable2;
        printOperation("%", variable1, variable2, resultVariable);

        variable1 = 10;
        variable2 = 1;


        System.out.println("\n>>, >>>, <<:");

        resultVariable = variable1 >> variable2;
        printOperation(">>", variable1, variable2, resultVariable);

        resultVariable = variable1 >>> variable2;
        printOperation(">>>", variable1, variable2, resultVariable);

        resultVariable = variable1 << variable2;
        printOperation("<<", variable1, variable2, resultVariable);

        variable1 = -10;
        variable2 = 1;

        resultVariable = variable1 >> variable2;
        printOperation(">>", variable1, variable2, resultVariable);

        resultVariable = variable1 >>> variable2;
        printOperation(">>>", variable1, variable2, resultVariable);

        resultVariable = variable1 << variable2;
        printOperation("<<", variable1, variable2, resultVariable);


        System.out.println("\n--, ++:");

        variable1 = 5;
        variable2 = variable1;

        variable1++;
        System.out.printf("variable1++: before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        ++variable1;
        System.out.printf("++variable1: before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1--;
        System.out.printf("variable1--: before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        --variable1;
        System.out.printf("--variable1: before: %d, after: %d\n", variable2, variable1);


        System.out.printf("Print 'variable1++' (variable1 == %d): %d\n", variable1, variable1++);
        System.out.printf("Print '++variable1' (variable1 == %d): %d\n", variable1, ++variable1);
        System.out.printf("Print 'variable1--' (variable1 == %d) %d:\n", variable1, variable1--);
        System.out.printf("Print '--variable1' (variable1 == %d): %d\n", variable1, --variable1);


        System.out.println("+=, -=, *=, /=, %=");
        variable1 = 100;

        variable2 = variable1;
        variable1+=5;
        System.out.printf("variable1+=5: variable1 before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1-=5;
        System.out.printf("variable1-=5: variable1 before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1*=5;
        System.out.printf("variable1*=5: variable1 before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1/=5;
        System.out.printf("variable1/=5: variable1 before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1%=5;
        System.out.printf("variable1%%=5: variable1 before: %d, after: %d\n", variable2, variable1);


        System.out.println("Types");
        variable1 = 10;

        byte shortVar = (byte) variable1;
        int intVar = variable1;
        long longVar = variable1;
        char charVar = (char)variable1;

        System.out.println("Short " + variable1 + "to short:" + shortVar);
        System.out.println("Short " + variable1 + " to int:" + intVar);
        System.out.println("Short " + variable1 + " to long:" + longVar);
        System.out.println("Short " + variable1 + " to char:" + charVar);

        System.out.println("Relational:");

        variable1 = 100;
        variable2 = 101;

        boolean boolResult = variable1 == variable2;
        printOperation("==", variable1, variable2, boolResult);

        boolResult = variable1 != variable2;
        printOperation("!=", variable1, variable2, boolResult);

        boolResult = variable1 >= variable2;
        printOperation(">=", variable1, variable2, boolResult);

        boolResult = variable1 <= variable2;
        printOperation("<=", variable1, variable2, boolResult);

        boolResult = variable1 > variable2;
        printOperation(">", variable1, variable2, boolResult);

        boolResult = variable1 < variable2;
        printOperation("<", variable1, variable2, boolResult);
    }

    public static void testCaseInt(){
        System.out.println("\n\n------Int test------");

        int variable1 = 10;
        int variable2 = 20;
        int resultVariable;


        System.out.println("\n +, -, *, /, %:");

        resultVariable = variable1 + variable2;
        printOperation("+", variable1, variable2, resultVariable);

        resultVariable = variable1 - variable2;
        printOperation("-", variable1, variable2, resultVariable);

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);

        resultVariable = variable1 / variable2;
        printOperation("/", variable1, variable2, resultVariable);

        resultVariable = variable1 % variable2;
        printOperation("%", variable1, variable2, resultVariable);

        resultVariable = variable1 & variable2;
        printOperation("&", variable1, variable2, resultVariable);

        resultVariable = variable1 ^ variable2;
        printOperation("^", variable1, variable2, resultVariable);

        resultVariable = variable1 | variable2;
        printOperation("|", variable1, variable2, resultVariable);



        System.out.println("\nwith zero:");

        variable1 = 10;
        variable2 = 0;

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);


        try {
            resultVariable = variable1 / variable2;
            printOperation("/", variable1, variable2, resultVariable);
        }
        catch (ArithmeticException e) { ;
            printOperationError("/", variable1, variable2, e.getMessage());
        }

        try {
            resultVariable = variable1 % variable2;
            printOperation("%", variable1, variable2, resultVariable);
        }
        catch (ArithmeticException e) {
            printOperationError("%", variable1, variable2, e.getMessage());
        }

        variable1 = 0;
        variable2 = 10;

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);

        resultVariable = variable1 / variable2;
        printOperation("/", variable1, variable2, resultVariable);

        resultVariable = variable1 % variable2;
        printOperation("%", variable1, variable2, resultVariable);

        variable1 = 10;
        variable2 = 1;


        System.out.println("\n>>, >>>, <<:");

        resultVariable = variable1 >> variable2;
        printOperation(">>", variable1, variable2, resultVariable);

        resultVariable = variable1 >>> variable2;
        printOperation(">>>", variable1, variable2, resultVariable);

        resultVariable = variable1 << variable2;
        printOperation("<<", variable1, variable2, resultVariable);

        variable1 = -10;
        variable2 = 1;

        resultVariable = variable1 >> variable2;
        printOperation(">>", variable1, variable2, resultVariable);

        resultVariable = variable1 >>> variable2;
        printOperation(">>>", variable1, variable2, resultVariable);

        resultVariable = variable1 << variable2;
        printOperation("<<", variable1, variable2, resultVariable);


        System.out.println("\n--, ++:");

        variable1 = 5;
        variable2 = variable1;

        variable1++;
        System.out.printf("variable1++: before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        ++variable1;
        System.out.printf("++variable1: before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1--;
        System.out.printf("variable1--: before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        --variable1;
        System.out.printf("--variable1: before: %d, after: %d\n", variable2, variable1);


        System.out.printf("Print 'variable1++' (variable1 == %d): %d\n", variable1, variable1++);
        System.out.printf("Print '++variable1' (variable1 == %d): %d\n", variable1, ++variable1);
        System.out.printf("Print 'variable1--' (variable1 == %d) %d:\n", variable1, variable1--);
        System.out.printf("Print '--variable1' (variable1 == %d): %d\n", variable1, --variable1);


        System.out.println("+=, -=, *=, /=, %=");
        variable1 = 100;

        variable2 = variable1;
        variable1+=5;
        System.out.printf("variable1+=5: variable1 before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1-=5;
        System.out.printf("variable1-=5: variable1 before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1*=5;
        System.out.printf("variable1*=5: variable1 before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1/=5;
        System.out.printf("variable1/=5: variable1 before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1%=5;
        System.out.printf("variable1%%=5: variable1 before: %d, after: %d\n", variable2, variable1);



        System.out.println("Types");
        variable1 = 10;

        byte byteVar = (byte)variable1;
        short shortVar = (short)variable1;
        long longVar = variable1;
        char charVar = (char)variable1;

        System.out.println("Int " + variable1 + "to byte:" + byteVar);
        System.out.println("Int " + variable1 + " to short:" + shortVar);
        System.out.println("Int " + variable1 + " to long:" + longVar);
        System.out.println("Int " + variable1 + " to char:" + charVar);

        System.out.println("Relational:");

        variable1 = 100;
        variable2 = 101;

        boolean boolResult = variable1 == variable2;
        printOperation("==", variable1, variable2, boolResult);

        boolResult = variable1 != variable2;
        printOperation("!=", variable1, variable2, boolResult);

        boolResult = variable1 >= variable2;
        printOperation(">=", variable1, variable2, boolResult);

        boolResult = variable1 <= variable2;
        printOperation("<=", variable1, variable2, boolResult);

        boolResult = variable1 > variable2;
        printOperation(">", variable1, variable2, boolResult);

        boolResult = variable1 < variable2;
        printOperation("<", variable1, variable2, boolResult);
    }

    public static void testCaseLong(){
        System.out.println("\n\n------Long test------");

        long variable1 = 10;
        long variable2 = 20;
        long resultVariable;


        System.out.println("\n +, -, *, /, %:");

        resultVariable = variable1 + variable2;
        printOperation("+", variable1, variable2, resultVariable);

        resultVariable = variable1 - variable2;
        printOperation("-", variable1, variable2, resultVariable);

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);

        resultVariable = variable1 / variable2;
        printOperation("/", variable1, variable2, resultVariable);

        resultVariable = variable1 % variable2;
        printOperation("%", variable1, variable2, resultVariable);

        resultVariable = variable1 & variable2;
        printOperation("&", variable1, variable2, resultVariable);

        resultVariable = variable1 ^ variable2;
        printOperation("^", variable1, variable2, resultVariable);

        resultVariable = variable1 | variable2;
        printOperation("|", variable1, variable2, resultVariable);


        System.out.println("\nwith zero:");

        variable1 = 10;
        variable2 = 0;

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);


        try {
            resultVariable = variable1 / variable2;
            printOperation("/", variable1, variable2, resultVariable);
        }
        catch (ArithmeticException e) { ;
            printOperationError("/", variable1, variable2, e.getMessage());
        }

        try {
            resultVariable = variable1 % variable2;
            printOperation("%", variable1, variable2, resultVariable);
        }
        catch (ArithmeticException e) {
            printOperationError("%", variable1, variable2, e.getMessage());
        }

        variable1 = 0;
        variable2 = 10;

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);

        resultVariable = variable1 / variable2;
        printOperation("/", variable1, variable2, resultVariable);

        resultVariable = variable1 % variable2;
        printOperation("%", variable1, variable2, resultVariable);

        variable1 = 10;
        variable2 = 1;


        System.out.println("\n>>, >>>, <<:");

        resultVariable = variable1 >> variable2;
        printOperation(">>", variable1, variable2, resultVariable);

        resultVariable = variable1 >>> variable2;
        printOperation(">>>", variable1, variable2, resultVariable);

        resultVariable = variable1 << variable2;
        printOperation("<<", variable1, variable2, resultVariable);

        variable1 = -10;
        variable2 = 1;

        resultVariable = variable1 >> variable2;
        printOperation(">>", variable1, variable2, resultVariable);

        resultVariable = variable1 >>> variable2;
        printOperation(">>>", variable1, variable2, resultVariable);

        resultVariable = variable1 << variable2;
        printOperation("<<", variable1, variable2, resultVariable);


        System.out.println("\n--, ++:");

        variable1 = 5;
        variable2 = variable1;

        variable1++;
        System.out.printf("variable1++: before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        ++variable1;
        System.out.printf("++variable1: before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1--;
        System.out.printf("variable1--: before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        --variable1;
        System.out.printf("--variable1: before: %d, after: %d\n", variable2, variable1);


        System.out.printf("Print 'variable1++' (variable1 == %d): %d\n", variable1, variable1++);
        System.out.printf("Print '++variable1' (variable1 == %d): %d\n", variable1, ++variable1);
        System.out.printf("Print 'variable1--' (variable1 == %d) %d:\n", variable1, variable1--);
        System.out.printf("Print '--variable1' (variable1 == %d): %d\n", variable1, --variable1);


        System.out.println("+=, -=, *=, /=, %=");
        variable1 = 100;

        variable2 = variable1;
        variable1+=5;
        System.out.printf("variable1+=5: variable1 before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1-=5;
        System.out.printf("variable1-=5: variable1 before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1*=5;
        System.out.printf("variable1*=5: variable1 before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1/=5;
        System.out.printf("variable1/=5: variable1 before: %d, after: %d\n", variable2, variable1);

        variable2 = variable1;
        variable1%=5;
        System.out.printf("variable1%%=5: variable1 before: %d, after: %d\n", variable2, variable1);


        System.out.println("Types");
        variable1 = 10;

        byte byteVar = (byte)variable1;
        short shortVar = (short)variable1;
        int intVar = (int)variable1;
        char charVar = (char)variable1;

        System.out.println("Long " + variable1 + "to byte:" + byteVar);
        System.out.println("Long " + variable1 + " to short:" + shortVar);
        System.out.println("Long " + variable1 + " to int:" + intVar);
        System.out.println("Long " + variable1 + " to char:" + charVar);


        System.out.println("Relational:");

        variable1 = 100;
        variable2 = 101;

        boolean boolResult = variable1 == variable2;
        printOperation("==", variable1, variable2, boolResult);

        boolResult = variable1 != variable2;
        printOperation("!=", variable1, variable2, boolResult);

        boolResult = variable1 >= variable2;
        printOperation(">=", variable1, variable2, boolResult);

        boolResult = variable1 <= variable2;
        printOperation("<=", variable1, variable2, boolResult);

        boolResult = variable1 > variable2;
        printOperation(">", variable1, variable2, boolResult);

        boolResult = variable1 < variable2;
        printOperation("<", variable1, variable2, boolResult);
    }

    public static void testCaseChar(){
        System.out.println("\n\n------Char test------");

        char variable1 = 'z';
        char variable2 = 'a';
        int resultVariable;


        System.out.println("\n +, -, *, /, %:");

        resultVariable = variable1 + variable2;
        printOperation("+", variable1, variable2, resultVariable);

        resultVariable = variable1 - variable2;
        printOperation("-", variable1, variable2, resultVariable);

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);

        resultVariable = variable1 / variable2;
        printOperation("/", variable1, variable2, resultVariable);

        resultVariable = variable1 % variable2;
        printOperation("%", variable1, variable2, resultVariable);

        resultVariable = variable1 & variable2;
        printOperation("&", variable1, variable2, resultVariable);

        resultVariable = variable1 ^ variable2;
        printOperation("^", variable1, variable2, resultVariable);

        resultVariable = variable1 | variable2;
        printOperation("|", variable1, variable2, resultVariable);


        System.out.println("\nwith zero:");

        variable1 = 50;
        variable2 = 0;

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);


        try {
            resultVariable = variable1 / variable2;
            printOperation("/", variable1, variable2, resultVariable);
        }
        catch (ArithmeticException e) { ;
            printOperationError("/", variable1, variable2, e.getMessage());
        }

        try {
            resultVariable = variable1 % variable2;
            printOperation("%", variable1, variable2, resultVariable);
        }
        catch (ArithmeticException e) {
            printOperationError("%", variable1, variable2, e.getMessage());
        }

        variable1 = 0;
        variable2 = 50;

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);

        resultVariable = variable1 / variable2;
        printOperation("/", variable1, variable2, resultVariable);

        resultVariable = variable1 % variable2;
        printOperation("%", variable1, variable2, resultVariable);

        variable1 = 10;
        variable2 = 1;


        System.out.println("\n>>, >>>, <<:");

        resultVariable = variable1 >> variable2;
        printOperation(">>", variable1, variable2, resultVariable);

        resultVariable = variable1 >>> variable2;
        printOperation(">>>", variable1, variable2, resultVariable);

        resultVariable = variable1 << variable2;
        printOperation("<<", variable1, variable2, resultVariable);

        variable1 = 10;
        variable2 = 1;

        resultVariable = variable1 >> variable2;
        printOperation(">>", variable1, variable2, resultVariable);

        resultVariable = variable1 >>> variable2;
        printOperation(">>>", variable1, variable2, resultVariable);

        resultVariable = variable1 << variable2;
        printOperation("<<", variable1, variable2, resultVariable);


        System.out.println("\n--, ++:");

        variable1 = 55;
        variable2 = variable1;

        variable1++;
        System.out.printf("variable1++: before: %c, after: %c\n", variable2, variable1);

        variable2 = variable1;
        ++variable1;
        System.out.printf("++variable1: before: %c, after: %c\n", variable2, variable1);

        variable2 = variable1;
        variable1--;
        System.out.printf("variable1--: before: %c, after: %c\n", variable2, variable1);

        variable2 = variable1;
        --variable1;
        System.out.printf("--variable1: before: %c, after: %c\n", variable2, variable1);


        System.out.printf("Print 'variable1++' (variable1 == %c): %c\n", variable1, variable1++);
        System.out.printf("Print '++variable1' (variable1 == %c): %c\n", variable1, ++variable1);
        System.out.printf("Print 'variable1--' (variable1 == %c) %c:\n", variable1, variable1--);
        System.out.printf("Print '--variable1' (variable1 == %c): %c\n", variable1, --variable1);


        System.out.println("+=, -=, *=, /=, %=");
        variable1 = 500;

        variable2 = variable1;
        variable1+=5;
        System.out.printf("variable1+=5: variable1 before: %c, after: %c\n", variable2, variable1);

        variable2 = variable1;
        variable1-=5;
        System.out.printf("variable1-=5: variable1 before: %c, after: %c\n", variable2, variable1);

        variable2 = variable1;
        variable1*=5;
        System.out.printf("variable1*=5: variable1 before: %c, after: %c\n", variable2, variable1);

        variable2 = variable1;
        variable1/=5;
        System.out.printf("variable1/=5: variable1 before: %c, after: %c\n", variable2, variable1);

        variable2 = variable1;
        variable1%=5;
        System.out.printf("variable1%%=5: variable1 before: %c, after: %c\n", variable2, variable1);


        System.out.println("Types");
        variable1 = 100;

        byte byteVar = (byte)variable1;
        short shortVar = (short)variable1;
        int intVar = (int)variable1;
        long longVar = (long)variable1;

        System.out.println("Char " + variable1 + " to byte:" + byteVar);
        System.out.println("Char " + variable1 + " to short:" + shortVar);
        System.out.println("Char " + variable1 + " to int:" + intVar);
        System.out.println("Char " + variable1 + " to long:" + longVar);


        System.out.println("Relational:");

        variable1 = 100;
        variable2 = 101;

        boolean boolResult = variable1 == variable2;
        printOperation("==", variable1, variable2, boolResult);

        boolResult = variable1 != variable2;
        printOperation("!=", variable1, variable2, boolResult);

        boolResult = variable1 >= variable2;
        printOperation(">=", variable1, variable2, boolResult);

        boolResult = variable1 <= variable2;
        printOperation("<=", variable1, variable2, boolResult);

        boolResult = variable1 > variable2;
        printOperation(">", variable1, variable2, boolResult);

        boolResult = variable1 < variable2;
        printOperation("<", variable1, variable2, boolResult);

    }

    public static void testCaseFloat(){
        System.out.println("\n\n------Float test------");

        float variable1 = 10.1f;
        float variable2 = 20;
        float resultVariable;


        System.out.println("\n +, -, *, /, %:");

        resultVariable = variable1 + variable2;
        printOperation("+", variable1, variable2, resultVariable);

        resultVariable = variable1 - variable2;
        printOperation("-", variable1, variable2, resultVariable);

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);

        resultVariable = variable1 / variable2;
        printOperation("/", variable1, variable2, resultVariable);

        resultVariable = variable1 % variable2;
        printOperation("%", variable1, variable2, resultVariable);



        System.out.println("\nwith zero:");

        variable1 = 10;
        variable2 = 0;

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);


        try {
            resultVariable = variable1 / variable2;
            printOperation("/", variable1, variable2, resultVariable);
        }
        catch (ArithmeticException e) { ;
            printOperationError("/", variable1, variable2, e.getMessage());
        }

        try {
            resultVariable = variable1 % variable2;
            printOperation("%", variable1, variable2, resultVariable);
        }
        catch (ArithmeticException e) {
            printOperationError("%", variable1, variable2, e.getMessage());
        }

        variable1 = 0;
        variable2 = 10;

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);

        resultVariable = variable1 / variable2;
        printOperation("/", variable1, variable2, resultVariable);

        resultVariable = variable1 % variable2;
        printOperation("%", variable1, variable2, resultVariable);

        variable1 = 10;
        variable2 = 1;


        System.out.println("\n--, ++:");

        variable1 = 5;
        variable2 = variable1;

        variable1++;
        System.out.printf("variable1++: before: %f, after: %f\n", variable2, variable1);

        variable2 = variable1;
        ++variable1;
        System.out.printf("++variable1: before: %f, after: %f\n", variable2, variable1);

        variable2 = variable1;
        variable1--;
        System.out.printf("variable1--: before: %f, after: %f\n", variable2, variable1);

        variable2 = variable1;
        --variable1;
        System.out.printf("--variable1: before: %f, after: %f\n", variable2, variable1);


        System.out.printf("Print 'variable1++' (variable1 == %f): %f\n", variable1, variable1++);
        System.out.printf("Print '++variable1' (variable1 == %f): %f\n", variable1, ++variable1);
        System.out.printf("Print 'variable1--' (variable1 == %f) %f:\n", variable1, variable1--);
        System.out.printf("Print '--variable1' (variable1 == %f): %f\n", variable1, --variable1);


        System.out.println("+=, -=, *=, /=, %=");
        variable1 = 100;

        variable2 = variable1;
        variable1+=5;
        System.out.printf("variable1+=5: variable1 before: %f, after: %f\n", variable2, variable1);

        variable2 = variable1;
        variable1-=5;
        System.out.printf("variable1-=5: variable1 before: %f, after: %f\n", variable2, variable1);

        variable2 = variable1;
        variable1*=5;
        System.out.printf("variable1*=5: variable1 before: %f, after: %f\n", variable2, variable1);

        variable2 = variable1;
        variable1/=5;
        System.out.printf("variable1/=5: variable1 before: %f, after: %f\n", variable2, variable1);

        variable2 = variable1;
        variable1%=5;
        System.out.printf("variable1%%=5: variable1 before: %f, after: %f\n", variable2, variable1);



        System.out.println("Types");
        variable1 = 10.5f;

        byte byteVar = (byte)variable1;
        short shortVar = (short)variable1;
        int intVar = (int)variable1;
        long longVar = (long)variable1;
        char charVar = (char)variable1;

        System.out.println("Float " + variable1 + "to byte:" + byteVar);
        System.out.println("Float " + variable1 + " to short:" + shortVar);
        System.out.println("Float " + variable1 + " to int:" + intVar);
        System.out.println("Float " + variable1 + " to long:" + longVar);
        System.out.println("Float " + variable1 + " to char:" + charVar);

        System.out.println("Relational:");

        variable1 = 100;
        variable2 = 101;

        boolean boolResult = variable1 == variable2;
        printOperation("==", variable1, variable2, boolResult);

        boolResult = variable1 != variable2;
        printOperation("!=", variable1, variable2, boolResult);

        boolResult = variable1 >= variable2;
        printOperation(">=", variable1, variable2, boolResult);

        boolResult = variable1 <= variable2;
        printOperation("<=", variable1, variable2, boolResult);

        boolResult = variable1 > variable2;
        printOperation(">", variable1, variable2, boolResult);

        boolResult = variable1 < variable2;
        printOperation("<", variable1, variable2, boolResult);
    }

    public static void testCaseDouble(){
        System.out.println("\n\n------Double test------");

        double variable1 = 10.1f;
        double variable2 = 20;
        double resultVariable;


        System.out.println("\n +, -, *, /, %:");

        resultVariable = variable1 + variable2;
        printOperation("+", variable1, variable2, resultVariable);

        resultVariable = variable1 - variable2;
        printOperation("-", variable1, variable2, resultVariable);

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);

        resultVariable = variable1 / variable2;
        printOperation("/", variable1, variable2, resultVariable);

        resultVariable = variable1 % variable2;
        printOperation("%", variable1, variable2, resultVariable);



        System.out.println("\nwith zero:");

        variable1 = 10;
        variable2 = 0;

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);


        try {
            resultVariable = variable1 / variable2;
            printOperation("/", variable1, variable2, resultVariable);
        }
        catch (ArithmeticException e) { ;
            printOperationError("/", variable1, variable2, e.getMessage());
        }

        try {
            resultVariable = variable1 % variable2;
            printOperation("%", variable1, variable2, resultVariable);
        }
        catch (ArithmeticException e) {
            printOperationError("%", variable1, variable2, e.getMessage());
        }

        variable1 = 0;
        variable2 = 10;

        resultVariable = variable1 * variable2;
        printOperation("*", variable1, variable2, resultVariable);

        resultVariable = variable1 / variable2;
        printOperation("/", variable1, variable2, resultVariable);

        resultVariable = variable1 % variable2;
        printOperation("%", variable1, variable2, resultVariable);

        variable1 = 10;
        variable2 = 1;


        System.out.println("\n--, ++:");

        variable1 = 5;
        variable2 = variable1;

        variable1++;
        System.out.printf("variable1++: before: %f, after: %f\n", variable2, variable1);

        variable2 = variable1;
        ++variable1;
        System.out.printf("++variable1: before: %f, after: %f\n", variable2, variable1);

        variable2 = variable1;
        variable1--;
        System.out.printf("variable1--: before: %f, after: %f\n", variable2, variable1);

        variable2 = variable1;
        --variable1;
        System.out.printf("--variable1: before: %f, after: %f\n", variable2, variable1);


        System.out.printf("Print 'variable1++' (variable1 == %f): %f\n", variable1, variable1++);
        System.out.printf("Print '++variable1' (variable1 == %f): %f\n", variable1, ++variable1);
        System.out.printf("Print 'variable1--' (variable1 == %f) %f:\n", variable1, variable1--);
        System.out.printf("Print '--variable1' (variable1 == %f): %f\n", variable1, --variable1);


        System.out.println("+=, -=, *=, /=, %=");
        variable1 = 100;

        variable2 = variable1;
        variable1+=5;
        System.out.printf("variable1+=5: variable1 before: %f, after: %f\n", variable2, variable1);

        variable2 = variable1;
        variable1-=5;
        System.out.printf("variable1-=5: variable1 before: %f, after: %f\n", variable2, variable1);

        variable2 = variable1;
        variable1*=5;
        System.out.printf("variable1*=5: variable1 before: %f, after: %f\n", variable2, variable1);

        variable2 = variable1;
        variable1/=5;
        System.out.printf("variable1/=5: variable1 before: %f, after: %f\n", variable2, variable1);

        variable2 = variable1;
        variable1%=5;
        System.out.printf("variable1%%=5: variable1 before: %f, after: %f\n", variable2, variable1);



        System.out.println("Types");
        variable1 = 10.5f;

        byte byteVar = (byte)variable1;
        short shortVar = (short)variable1;
        int intVar = (int)variable1;
        long longVar = (long)variable1;
        char charVar = (char)variable1;

        System.out.println("Double " + variable1 + "to byte:" + byteVar);
        System.out.println("Double " + variable1 + " to short:" + shortVar);
        System.out.println("Double " + variable1 + " to int:" + intVar);
        System.out.println("Double " + variable1 + " to long:" + longVar);
        System.out.println("Double " + variable1 + " to char:" + charVar);

        System.out.println("Relational:");

        variable1 = 100;
        variable2 = 101;

        boolean boolResult = variable1 == variable2;
        printOperation("==", variable1, variable2, boolResult);

        boolResult = variable1 != variable2;
        printOperation("!=", variable1, variable2, boolResult);

        boolResult = variable1 >= variable2;
        printOperation(">=", variable1, variable2, boolResult);

        boolResult = variable1 <= variable2;
        printOperation("<=", variable1, variable2, boolResult);

        boolResult = variable1 > variable2;
        printOperation(">", variable1, variable2, boolResult);

        boolResult = variable1 < variable2;
        printOperation("<", variable1, variable2, boolResult);
    }

    public static void testCaseBool(){
        System.out.println("\n\n-----Bool test------");
        boolean variable1 = false;
        boolean variable2 = true;
        boolean resultVar;

        resultVar = variable1 & variable2;
        printOperation("&", variable1, variable2, resultVar);

        resultVar = variable1 ^ variable2;
        printOperation("^", variable1, variable2, resultVar);

        resultVar = variable1 | variable2;
        printOperation("|", variable1, variable2, resultVar);

        resultVar = variable1 && variable2;
        printOperation("&&", variable1, variable2, resultVar);

        resultVar = variable1 || variable2;
        printOperation("||", variable1, variable2, resultVar);


        variable1 &= variable2;
        variable1 ^= variable2;
        variable1 |= variable2;
    }

    public static void testCaseString(){
        System.out.println("-----String test------");

        String variable1 = "Hello";
        String variable2 = "e";

        String resultVar;

        resultVar = variable1 + variable2;
        printOperation("+", variable1, variable2, resultVar);

        variable1 = resultVar;
        resultVar += variable2;
        printOperation("+=", variable1, variable2, resultVar);

        boolean resultBool = variable1 == variable2;
        printOperation("==", variable1, variable2, resultBool);

        resultBool = variable1 != variable2;
        printOperation("!=", variable1, variable2, resultBool);

    }

    public static void testCaseObject(){
        System.out.println("\n\n----Object test----");

        MyClass variable1 = new MyClass(3);
        MyClass variable2 = new MyClass(3);
        MyClass variable3 = new MyClass(5);

        Object objectVar;

        boolean resultVar;


        resultVar = variable1 == variable2;
        System.out.printf("variable1(%s) == variable2(%s): %b\n", variable1.toString(), variable2.toString(), resultVar);

        resultVar = variable1.equals(variable2);
        System.out.printf("variable1(%s) equals variable2(%s): %b\n", variable1.toString(), variable2.toString(), resultVar);

        resultVar = variable1.equals(variable3);
        System.out.printf("variable1(%s) equals variable3(%s): %b\n", variable1.toString(), variable3.toString(), resultVar);

    }

    public static void printOperation(String operation, long variable1, long variable2, long resultVariable){
        System.out.printf("variable1 %s variable2: %d %s %d = %d\n", operation, variable1, operation, variable2, resultVariable);
    }

    public static void printOperation(String operation, double variable1, double variable2, double resultVariable){
        System.out.printf("variable1 %s variable2: %f %s %f = %f\n", operation, variable1, operation, variable2, resultVariable);
    }

    public static void printOperation(String operation, long variable1, long variable2, boolean resultVariable){
        System.out.printf("variable1 %s variable2: %d %s %d = %b\n", operation, variable1, operation, variable2, resultVariable);
    }

    public static void printOperation(String operation, double variable1, double variable2, boolean resultVariable){
        System.out.printf("variable1 %s variable2: %f %s %f = %b\n", operation, variable1, operation, variable2, resultVariable);
    }

    public static void printOperation(String operation, boolean variable1, boolean variable2, boolean resultVariable){
        System.out.printf("variable1 %s variable2: %b %s %b = %b\n", operation, variable1, operation, variable2, resultVariable);
    }

    public static void printOperation(String operation, String variable1, String variable2, boolean resultVariable){
        System.out.printf("variable1 %s variable2: %s %s %s = %b\n", operation, variable1, operation, variable2, resultVariable);
    }

    public static void printOperation(String operation, String variable1, String variable2, String resultVariable){
        System.out.printf("variable1 %s variable2: %s %s %s = %s\n", operation, variable1, operation, variable2, resultVariable);
    }

    public static void printOperationError(String operation, long variable1, long variable2, String error){
        System.out.printf("variable1 %s variable2: %d %s %d => Error: %s\n", operation, variable1, operation, variable2, error);
    }

    public static void printOperationError(String operation, double variable1, double variable2, String error){
        System.out.printf("variable1 %s variable2: %f %s %f => Error: %s\n", operation, variable1, operation, variable2, error);
    }

}


class MyClass{
    public int number;

    MyClass(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "MyObject with number: " + number;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != this.getClass()){
            return false;
        }

        return ((MyClass) obj).number == this.number;
    }
}


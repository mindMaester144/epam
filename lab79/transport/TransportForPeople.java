package lab79.transport;

public class TransportForPeople extends Transport {
    private int maxPassengersCount;
    private int currentPassengersCount;

    public TransportForPeople() {
        maxPassengersCount = 0;
        currentPassengersCount = 0;
    }

    public TransportForPeople(String id, String brand, String model, double speed,
                              double length, double width, double weight, double cost,
                              int maxPassengersCount) {
        super(id, brand, model, speed, length, width, weight, cost);
        this.maxPassengersCount = maxPassengersCount;
        currentPassengersCount = 0;
    }

    public boolean addPassenger(){
        if (currentPassengersCount < maxPassengersCount) {
            currentPassengersCount++;
            return true;
        }

        return false;
    }

    public boolean removePassenger(){
        if (currentPassengersCount > 0){
            currentPassengersCount--;
            return true;
        }

        return false;
    }
}

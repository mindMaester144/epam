package lab79.transport;

public class Coupe extends TransportForPeople {
    public Coupe() {

    }

    public Coupe(String id, String brand, String model, double speed,
                       double length, double width, double weight, double cost,
                       int maxPassengersCount) {
        super(id, brand, model, speed, length, width, weight, cost, maxPassengersCount);
    }
}

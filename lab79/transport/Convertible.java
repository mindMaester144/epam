package lab79.transport;

public class Convertible extends TransportForPeople {
    public Convertible() {

    }

    public Convertible(String id, String brand, String model, double speed,
                              double length, double width, double weight, double cost,
                              int maxPassengersCount) {
        super(id, brand, model, speed, length, width, weight, cost, maxPassengersCount);
    }
}

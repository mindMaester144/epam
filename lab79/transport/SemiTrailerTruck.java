package lab79.transport;

public class SemiTrailerTruck extends TransportForStuff {
    public SemiTrailerTruck() {

    }

    public SemiTrailerTruck(String id, String brand, String model, double speed,
                     double length, double width, double weight, double cost,
                     int maxStuffCount) {
        super(id, brand, model, speed, length, width, weight, cost, maxStuffCount);
    }
}

package lab79.transport;

public class Sedan extends TransportForPeople {
    public Sedan() {

    }

    public Sedan(String id, String brand, String model, double speed,
                 double length, double width, double weight, double cost,
                 int maxPassengersCount) {
        super(id, brand, model, speed, length, width, weight, cost, maxPassengersCount);
    }
}

package lab79.transport;

public class TransportForStuff extends Transport {
    private double maxStuffWeight;
    private double currentStuffWeight;


    public TransportForStuff() {
        maxStuffWeight = 0;
        currentStuffWeight = 0;
    }

    public TransportForStuff(String id, String brand, String model, double speed,
                              double length, double width, double weight, double cost,
                              int maxStuffWeight) {
        super(id, brand, model, speed, length, width, weight, cost);
        this.maxStuffWeight = maxStuffWeight;
        currentStuffWeight = 0;
    }

    public boolean addStuff(double stuffToAddWeight){
        if (currentStuffWeight + stuffToAddWeight <= maxStuffWeight) {
            currentStuffWeight += stuffToAddWeight;
            return true;
        }

        return false;
    }

    public boolean removeStuff(double stuffToRemoveWeight){
        if (currentStuffWeight - stuffToRemoveWeight >= 0){
            currentStuffWeight -= stuffToRemoveWeight;
            return true;
        }

        return false;
    }
}

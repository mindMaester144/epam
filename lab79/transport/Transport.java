package lab79.transport;

public class Transport {
    private String id;
    private String brand;
    private String model;

    private double speed;
    private double length;
    private double width;
    private double weight;
    private double cost;

    public Transport() {
        id = "Unknown";
        brand = "Unknown";
        model = "Unknown";
    }

    public Transport(String id, String brand, String model, double speed,
                     double length, double width, double weight, double cost) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.speed = speed;
        this.length = length;
        this.width = width;
        this.weight = weight;
        this.cost = cost;
    }


    public double getPathTime(double pathLength) {
        return pathLength / speed;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return brand + ": " + model + " (" + id + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Transport transport = (Transport) obj;
        return (id == transport.id || (id != null && id.equals(transport.getId())))
                && (brand == transport.brand || (brand != null && brand.equals(transport.getBrand())))
                && (model == transport.model || (model != null && model.equals(transport.getModel())))
                && speed == transport.getSpeed()
                && length == transport.getLength()
                && width == transport.getWidth()
                && weight == transport.getWeight()
                && cost == transport.getCost();
    }

    @Override
    public int hashCode() {
        return (getId() + "|" + getModel() + "|" + getBrand() + "|" + getSpeed() + "|" + getLength()
                + "|" + getWidth() + "|" + getWeight() + "|" + getCost()).hashCode();
    }
}

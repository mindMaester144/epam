package lab79;

import lab79.modules.MyContainer;
import lab79.modules.TransportFinder;
import lab79.modules.TransportSorter;
import lab79.transport.*;

public class Garage {
    private MyContainer<Transport> transportContainer;
    private TransportFinder transportFinder;
    private TransportSorter transportSorter;

    public Garage() {
        transportContainer = new MyContainer<Transport>();
        transportFinder = new TransportFinder();
        transportSorter = new TransportSorter();
    }

    public Garage(MyContainer<Transport> transportContainer,
                  TransportFinder transportFinder,
                  TransportSorter transportSorter) {
        this.transportContainer = transportContainer;
        this.transportFinder = transportFinder;
        this.transportSorter = transportSorter;
    }


    public void addTransport(Transport transport) {
        transportContainer.add(transport);
    }

    public void addTransport(Transport[] transport) {
        transportContainer.add(transport);
    }

    public void addConvertible(String id, String brand, String model, double speed,
                               double length, double width, double weight, double cost,
                               int maxPassengersCount) {

        transportContainer.add(new Convertible(id, brand, model, speed, length, width, weight, cost, maxPassengersCount));
    }

    public void addCoupe(String id, String brand, String model, double speed,
                         double length, double width, double weight, double cost,
                         int maxPassengersCount) {
        transportContainer.add(new Coupe(id, brand, model, speed, length, width, weight, cost, maxPassengersCount));
    }

    public void addDumpTruck(String id, String brand, String model, double speed,
                             double length, double width, double weight, double cost,
                             int maxStuffWeight) {
        transportContainer.add(new DumpTruck(id, brand, model, speed, length, width, weight, cost, maxStuffWeight));
    }

    public void addSedan(String id, String brand, String model, double speed,
                         double length, double width, double weight, double cost,
                         int maxPassengersCount) {
        transportContainer.add(new Sedan(id, brand, model, speed, length, width, weight, cost, maxPassengersCount));
    }

    public void addSemiTrailerTruck(String id, String brand, String model, double speed,
                                    double length, double width, double weight, double cost,
                                    int maxStuffWeight) {
        transportContainer.add(new SemiTrailerTruck(id, brand, model, speed, length, width, weight, cost, maxStuffWeight));
    }

    public void addVan(String id, String brand, String model, double speed,
                       double length, double width, double weight, double cost,
                       int maxStuffWeight) {
        transportContainer.add(new Van(id, brand, model, speed, length, width, weight, cost, maxStuffWeight));
    }


    public boolean removeTransport(Object object) {
        return transportContainer.remove(object);
    }

    public boolean removeTransport(Object[] objects) {
        return transportContainer.remove(objects);
    }

    public Transport[] findTransportById(String id) {
        return transportFinder.findTransportById(transportContainer, id);
    }

    public Transport[] findTransportByModel(String model) {
        return transportFinder.findTransportByModel(transportContainer, model);
    }

    public Transport[] findTransportByBrand(String brand) {
        return transportFinder.findTransportByBrand(transportContainer, brand);
    }

    public Transport[] findTransportByCost(double cost) {
        return transportFinder.findTransportByCost(transportContainer, cost);
    }

    public Transport[] findTransportCostLess(double cost) {
        return transportFinder.findTransportCostLess(transportContainer, cost);
    }

    public Transport[] findTransportCostMore(double cost) {
        return transportFinder.findTransportCostMore(transportContainer, cost);
    }

    public Transport[] getTransport() {
        return transportContainer.getMas();
    }

    public Transport[] getTransportSortedByCost() {
        return transportSorter.getSortedByCostBubble(transportContainer);
    }

    public TransportForPeople[] getTransportForPeople() {
        return transportFinder.findTransportForPeople(transportContainer);
    }

    public TransportForStuff[] getTransportForStuff() {
        return transportFinder.findTransportForStuff(transportContainer);
    }
}

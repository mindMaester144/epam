package lab79;

import lab79.modules.MyContainer;
import lab79.transport.*;

import java.util.Arrays;

public class Tests {
    public static void main(String[] argv) {
        testTask();
    }

    public static void testTask() {
        MyContainer<Transport> garage = new MyContainer<Transport>();

        Coupe coupe = new Coupe();
        coupe.setBrand("Brand 1");
        coupe.setModel("Model 1");
        coupe.setId("Id 1");
        coupe.setCost(10);

        Convertible convertible = new Convertible();
        convertible.setBrand("Brand 2");
        convertible.setModel("Model 2");
        convertible.setId("id 2");
        convertible.setCost(10);

        Sedan sedan = new Sedan();
        sedan.setBrand("Brand 3");
        sedan.setModel("Model 3");
        sedan.setId("Id 3");
        sedan.setCost(10);

        DumpTruck dumpTruck = new DumpTruck();
        dumpTruck.setBrand("Brand 4");
        dumpTruck.setModel("Model 4");
        dumpTruck.setId("Id 4");
        dumpTruck.setCost(10);

        SemiTrailerTruck semiTrailerTruck = new SemiTrailerTruck();
        semiTrailerTruck.setBrand("Brand 5");
        semiTrailerTruck.setModel("Model 5");
        semiTrailerTruck.setId("Id 5");
        semiTrailerTruck.setCost(10);

        Van van = new Van();
        van.setBrand("Brand 6");
        van.setModel("Model 6");
        van.setId("Id 6");
        van.setCost(10);

        garage.add(coupe);
        garage.add(convertible);
        garage.add(sedan);
        garage.add(dumpTruck);
        garage.add(semiTrailerTruck);
        garage.add(van);

        System.out.println("Transport in garage: " + Arrays.toString(garage.getMas()));

        System.out.println("Try to remove from garage: " + garage.remove(van));
        System.out.println("Transport in garage: " + Arrays.toString(garage.getMas()));

        System.out.println("Try to add second time ");
        garage.add(semiTrailerTruck);

        System.out.println("Try to find existing object: " + garage.checkHas(coupe));

        System.out.println("Try to find not existing object: " + garage.checkHas(van));

        System.out.println("Get transport count: " + garage.getElementsCount());
    }
}

package lab79.modules;

import lab79.transport.Transport;
import lab79.transport.TransportForPeople;
import lab79.transport.TransportForStuff;

public class TransportFinder {
    public Transport[] findTransportById(MyContainer<Transport> container, String id) {
        return findTransportById(container.getMas(), id);
    }

    public Transport[] findTransportByBrand(MyContainer<Transport> container, String brand) {
        return findTransportByBrand(container.getMas(), brand);
    }

    public Transport[] findTransportByModel(MyContainer<Transport> container, String model) {
        return findTransportByModel(container.getMas(), model);
    }

    public Transport[] findTransportByCost(MyContainer<Transport> container, double cost){
        return findTransportByCost(container.getMas(), cost);
    }

    public Transport[] findTransportCostMore(MyContainer<Transport> container, double cost) {
        return findTransportCostMore(container.getMas(), cost);
    }

    public Transport[] findTransportCostLess(MyContainer<Transport> container, double cost) {
        return findTransportCostLess(container.getMas(), cost);
    }

    public TransportForPeople[] findTransportForPeople(MyContainer<Transport> container) {
        return findTransportForPeople(container.getMas());
    }

    public TransportForStuff[] findTransportForStuff(MyContainer<Transport> container) {
        return findTransportForStuff(container.getMas());
    }

    public Transport[] findTransportById(Transport[] transportMas, String id){
        MyContainer<Transport> transportContainer = new MyContainer<Transport>();

        for (Transport transport: transportMas) {
            if (transport.getId().equals(id)) {
                transportContainer.add(transport);
            }
        }

        return transportContainer.getMas();
    }

    public Transport[] findTransportByModel(Transport[] transportMas, String model){
        MyContainer<Transport> transportContainer = new MyContainer<Transport>();

        for (Transport transport: transportMas) {
            if (transport.getModel().equals(model)) {
                transportContainer.add(transport);
            }
        }

        return transportContainer.getMas();
    }

    public Transport[] findTransportByBrand(Transport[] transportMas, String brand){
        MyContainer<Transport> transportContainer = new MyContainer<Transport>();

        for (Transport transport: transportMas) {
            if (transport.getBrand().equals(brand)) {
                transportContainer.add(transport);
            }
        }

        return transportContainer.getMas();
    }


    public Transport[] findTransportByCost(Transport[] transportMas, double cost){
        MyContainer<Transport> transportContainer = new MyContainer<Transport>();

        for (Transport transport: transportMas) {
            if (transport.getCost() == cost) {
                transportContainer.add(transport);
            }
        }

        return transportContainer.getMas();
    }


    public Transport[] findTransportCostMore(Transport[] transportMas, double cost) {
        MyContainer<Transport> transportContainer = new MyContainer<Transport>();

        for (Transport transport: transportMas) {
            if (transport.getCost() > cost) {
                transportContainer.add(transport);
            }
        }

        return transportContainer.getMas();
    }

    public Transport[] findTransportCostLess(Transport[] transportMas, double cost) {
        MyContainer<Transport> transportContainer = new MyContainer<Transport>();

        for (Transport transport: transportMas) {
            if (transport.getCost() < cost) {
                transportContainer.add(transport);
            }
        }

        return transportContainer.getMas();
    }

    public TransportForPeople[] findTransportForPeople(Transport[] transportMas) {
        MyContainer<Transport> transportContainer = new MyContainer<Transport>();

        for (Transport transport: transportMas) {
            if (transport instanceof TransportForPeople) {
                transportContainer.add(transport);
            }
        }

        return (TransportForPeople[])transportContainer.getMas();
    }

    public TransportForStuff[] findTransportForStuff(Transport[] transportMas) {
        MyContainer<Transport> transportContainer = new MyContainer<Transport>();

        for (Transport transport: transportMas) {
            if (transport instanceof TransportForStuff) {
                transportContainer.add(transport);
            }
        }

        return (TransportForStuff[])transportContainer.getMas();
    }


}

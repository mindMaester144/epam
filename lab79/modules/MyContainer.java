package lab79.modules;


public class MyContainer<T> {
    private T[] array ;
    private int elementsCount;

    public MyContainer() {
        array = (T[]) new Object[10];
        elementsCount = 0;
    }

    public MyContainer(T[] elements) {
        array = elements;
        elementsCount = elements.length;
    }

    public MyContainer(Iterable<T> iterable) {
        array = (T[]) new Object[10];
        elementsCount = 0;

        for (T elem: iterable) {
            add(elem);
        }
    }

    public void add(T[] elementsMas) {
        for (T element: elementsMas) {
            add(element);
        }
    }

    public void add(Iterable<T> elements) {
        for (T element: elements) {
            add(element);
        }
    }

    public void add(T element) {
        if (elementsCount == array.length) {
            T[] newArray = (T[]) new Object[array.length * 2];

            for (int i = 0; i < elementsCount; i++) {
                newArray[i] = array[i];
            }

            array = newArray;
        }

        array[elementsCount] = element;
        elementsCount++;
    }

    public boolean remove(Object[] elementsMas) {
        boolean result = true;

        for (Object element: elementsMas) {
            if (!remove(element)) {
                result = false;
            }
        }

        return result;
    }

    public boolean remove(Object element) {
        for (int i = 0; i < elementsCount; i++) {
            if (element.equals(array[i])) {
                array[i] = array[elementsCount - 1];
                elementsCount--;

                return true;
            }
        }

        return false;
    }

    public boolean checkHas(Object element) {
        for (int i = 0; i < elementsCount; i++) {
            if (element.equals(array[i])) {
                return true;
            }
        }

        return false;
    }

    public T[] getMas() {
        T[] masToReturn = (T[]) new Object[elementsCount];

        for (int i = 0; i < elementsCount; i++) {
            masToReturn[i] = array[i];
        }

        return masToReturn;
    }

    public int getElementsCount() {
        return elementsCount;
    }

    public void empty() {
        array = (T[]) new Object[10];
        elementsCount = 0;
    }
}

package lab79.modules;

import lab79.transport.Transport;

public class TransportSorter {

    public Transport[] getSortedByCostBubble(MyContainer<Transport> container) {
        Transport[] masToSort = container.getMas();

        return sortCostBubble(masToSort);
    }

    public Transport[] getSortedByCostBubble(Transport[] mas) {
        Transport[] masToSort = new Transport[mas.length];
        System.arraycopy(mas, 0, masToSort, 0, mas.length);

        return sortCostBubble(masToSort);
    }


    public Transport[] getSortedByCostGnome(MyContainer<Transport> container) {
        Transport[] masToSort = container.getMas();

        return sortGnome(masToSort);
    }

    public Transport[] getSortedByCostGnime(Transport[] mas) {
        Transport[] masToSort = new Transport[mas.length];
        System.arraycopy(mas, 0, masToSort, 0, mas.length);

        return sortGnome(masToSort);
    }



    private Transport[] sortCostBubble(Transport[] masToSort) {
        for (int i = masToSort.length - 1; i > 0; i++) {
            for (int j = 0; j < i; j++) {
                if (masToSort[j].getCost() > masToSort[j + 1].getCost()) {
                    Transport x = masToSort[j];
                    masToSort[j] = masToSort[j + 1];
                    masToSort[j + 1] = x;
                }
            }
        }

        return masToSort;
    }

    private Transport[] sortGnome(Transport[] masToSort) {
        for (int i = 0; i < masToSort.length - 1; i++) {
            if (masToSort[i].getCost() > masToSort[i + 1].getCost()) {
                Transport x = masToSort[i];
                masToSort[i] = masToSort[i + 1];
                masToSort[i + 1] = x;

                if (i > 0) {
                    i -= 2;
                }
            }
        }

        return masToSort;
    }
}

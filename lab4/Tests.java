package lab4;

public class Tests {
    public static void main(String[] argv) {
        testTask1();
        testTask2();
        testTask3();
    }

    public static void testTask1() {
        System.out.println("\nTask1:");

        String flipResult = Task1.flipCoinSeveralTimes(3000);

        System.out.println(flipResult);
    }

    public static void testTask2() {
        System.out.println("\nTask2:");

        int resultMaxNumeral = Task2.findMaxNumeral(12345);
        System.out.printf("Max numeral: %d\n", resultMaxNumeral);

        boolean resultIsPalindrome = Task2.checkIfPalindrome(12321);
        System.out.printf("Is palindrome: %b\n", resultIsPalindrome);

        boolean resultIsPrime = Task2.checkIfPrime(12321);
        System.out.printf("Is prime: %b\n", resultIsPrime);

        String resultPrimeDividers = Task2.findAllPrimeDivider(136);
        System.out.printf("All prime dividers: %s\n", resultPrimeDividers);

        String resultILCMAndGCD = Task2.findLCMAndGCD(1234, 12345);
        System.out.printf("findLCMAndGCD: %s\n", resultILCMAndGCD);

        int resultDifferentNumeralsCount = Task2.findDifferentNumeralsCount(1234550);
        System.out.printf("DifferentNumeralsCount: %d\n", resultDifferentNumeralsCount);
    }

    public static void testTask3() {
        System.out.println("\nTask3:");

        boolean resultIsPerfect = Task3.checkIfPerfect(1333);
        System.out.printf("Is perfect result: %b\n", resultIsPerfect);
    }
}

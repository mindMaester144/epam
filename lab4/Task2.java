package lab4;

class Task2 {
    public static int findMaxNumeral(int number) {
        int maxNumeral = number % 10;

        while (number != 0) {
            maxNumeral = (number /= 10) % 10 > maxNumeral ? number % 10 : maxNumeral;
        }

        return maxNumeral;
    }

    public static boolean checkIfPalindrome(int number) {
        int numberCopy = number;
        int invertedNumber = 0;

        while (numberCopy > 0) {
            invertedNumber = invertedNumber * 10 + (numberCopy % 10);
            numberCopy /= 10;
        }

        return number == invertedNumber;
    }

    public static boolean checkIfPrime(int number) {
        if (number % 2 == 0) {
            return number == 2;
        }

        double maxNumberToCheck = Math.sqrt(number);
        int variableToDivide = 1;

        while (maxNumberToCheck >= variableToDivide) {
            variableToDivide += 2;

            if (number % variableToDivide == 0){
                return false;
            }
        }

        return true;
    }

    public static String findAllPrimeDivider(int number) {
        StringBuilder primeDividers = new StringBuilder();

        boolean isPrime;

        for (int i=2; i <= number; i++) {
            isPrime = true;

            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    isPrime = false;
                }
            }

            if (isPrime && number % i == 0) {
                primeDividers.append(i).append(" ");
            }
        }

        return primeDividers.toString();
    }

    public static String findLCMAndGCD(int number1, int number2){
        int resultLCM = findLCM(number1, number2);
        int resultGCD = findGCM(number1, number2);

        return "LCM: " + resultLCM + ", GCD: " + resultGCD;
    }

    private static int findLCM(int number1, int number2){
        int number3 = number1 * number2;

        while (number1 != 0 && number2 != 0) {
            if (number1 > number2) {
                number1 %= number2;
            }
            else {
                number2 %= number1;
            }
        }

        return number3 / (number1 + number2);
    }

    private static int findGCM(int number1, int number2){
        while (number1 != number2) {
            if (number1 > number2) {
                number1 -= number2;
            }
            else {
                number2 -= number1;
            }
        }

        return number1;
    }

    public static int findDifferentNumeralsCount(int number) {
        boolean isZero = false;
        boolean isOne = false;
        boolean isTwo = false;
        boolean isThree = false;
        boolean isFour = false;
        boolean isFive = false;
        boolean isSix = false;
        boolean isSeven = false;
        boolean isEight = false;
        boolean isNine = false;

        int result = 0;

        while (number != 0) {
            switch (number % 10) {
                case 0:
                    if (!isZero) {
                        isZero = true;
                        result++;
                    }
                    break;
                case 1:
                    if (!isOne) {
                        isOne = true;
                        result++;
                    }
                    break;
                case 2:
                    if (!isTwo) {
                        isTwo = true;
                        result++;
                    }
                    break;
                case 3:
                    if (!isThree) {
                        isThree = true;
                        result++;
                    }
                    break;
                case 4:
                    if (!isFour) {
                        isFour = true;
                        result++;
                    }
                    break;
                case 5:
                    if (!isFive) {
                        isFive = true;
                        result++;
                    }
                    break;
                case 6:
                    if (!isSix) {
                        isSix = true;
                        result++;
                    }
                    break;
                case 7:
                    if (!isSeven) {
                        isSeven = true;
                        result++;
                    }
                    break;
                case 8:
                    if (!isEight) {
                        isEight = true;
                        result++;
                    }
                    break;
                case 9:
                    if (!isNine) {
                        isNine = true;
                        result++;
                    }
                    break;
            }

            number /= 10;
        }

        return result;
    }

}

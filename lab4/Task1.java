package lab4;

import java.util.Random;

class Task1 {
    public static String flipCoinSeveralTimes(int timesToFlip) {
        Random random = new Random();
        int eagleCount = 0;


        for (int i = 0; i < timesToFlip; i++) {
            if (random.nextBoolean()) {
                eagleCount++;
            }
        }

        int tailsCount = timesToFlip - eagleCount;

        return "Flipped: " + timesToFlip + " times, eagles: " + eagleCount + ", tails: " + tailsCount;
    }

}

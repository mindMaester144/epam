package lab4;

class Task3 {
    public static boolean checkIfPerfect(int number) {
        int dividersSum = 1;

        for (int i=2; i < number; i++) {
            if (number % i == 0) {
                dividersSum += i;
            }
        }

        return number == dividersSum;
    }
}

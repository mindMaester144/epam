package lab8;

public class Task {
    public static boolean isPalindrome(String str) {// O(n)
        MyStackMas<Character> stack = new MyStackMas<>();
        int length = str.length();

        for (int i = 0; i < length; i++) {
            if (i == length / 2 && length % 2 == 1) {
                continue;
            }

            if (i < length / 2) {
                stack.push(str.charAt(i));
            }
            else {
                if (str.charAt(i) != stack.pop()) {
                    return false;
                }
            }
        }

        return true;
    }
}

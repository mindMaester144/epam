package lab8;

import java.util.NoSuchElementException;

public class MyLinkedList<T> {
    class MyListElement {
        T value;
        MyListElement nextElement;
        MyListElement prevElement;

        MyListElement(T value, MyListElement nextElement, MyListElement prevElement) {
            this.value = value;
            this.nextElement = nextElement;
            this.prevElement = prevElement;
        }

    }

    private MyListElement firstElement;
    private MyListElement lastElement;
    private int elementsCount;

    public MyLinkedList() {
        firstElement = null;
        lastElement = null;
        elementsCount = 0;
    }

    public MyLinkedList(T[] elements) {
        firstElement = null;
        lastElement = null;
        elementsCount = 0;

        for (T element: elements) {
            put(element);
        }
    }

    public void append(T element) {
        if (!isEmpty()) {
            MyListElement newListElement = new MyListElement(element, lastElement, null);
            lastElement.prevElement = newListElement;
            lastElement = newListElement;
        }
        else {
            inputFirstElement(element);
        }
    }

    public void put(T element) {
        if (!isEmpty()) {
            MyListElement newListElement = new MyListElement(element, null, lastElement);
            firstElement.nextElement = newListElement;
            firstElement = newListElement;
        }
        else {
            inputFirstElement(element);
        }
    }

    public T removeFirstAndReturn() throws NoSuchElementException{
        T result = first();
        removeFirst();

        return result;
    }

    public T removeLastAndReturn() throws NoSuchElementException{
        T result = last();
        removeLast();

        return result;
    }


    public void removeFirst() {
        if (!isEmpty()) {
            if (size() > 1) {
                firstElement = firstElement.prevElement;
                firstElement.nextElement = null;
            }
            else {
                firstElement = null;
                lastElement = null;
            }
            elementsCount--;
        }
    }

    public void removeLast() {
        if (!isEmpty()) {
            if (size() > 1) {
                lastElement = lastElement.nextElement;
                lastElement.prevElement = null;
            }
            else {
                firstElement = null;
                lastElement = null;
            }
            elementsCount--;
        }
    }


    public T first() throws NoSuchElementException{
        if (!isEmpty()) {
            return firstElement.value;
        }

        throw new NoSuchElementException("List is empty!");
    }

    public T last() throws NoSuchElementException{
        if (!isEmpty()) {
            return lastElement.value;
        }

        throw new NoSuchElementException("List is empty!");
    }


    private void inputFirstElement(T element) {
        MyListElement newListElement = new MyListElement(element, null, null);
        firstElement = newListElement;
        lastElement = newListElement;
    }


    public boolean isEmpty() {
        return elementsCount == 0;
    }


    public int size() {
        return elementsCount;
    }
}

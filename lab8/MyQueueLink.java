package lab8;

import java.util.NoSuchElementException;

public class MyQueueLink<T> {
    private MyLinkedList<T> linkedList;
    private int maxSize;

    public MyQueueLink() {
        linkedList = new MyLinkedList<T>();
        maxSize = -1;
    }

    public MyQueueLink(T[] elements) {
        linkedList = new MyLinkedList<T>(elements);
        maxSize = -1;
    }

    public MyQueueLink(int maxSize) throws RuntimeException{
        if(maxSize < 0) {
            throw new RuntimeException("Size can't be negative!");
        }

        linkedList = new MyLinkedList<T>();
        this.maxSize = maxSize;
    }

    public MyQueueLink(T[] elements, int maxSize) throws RuntimeException{
        if(maxSize < 0) {
            throw new RuntimeException("Size can't be negative!");
        }

        linkedList = new MyLinkedList<T>();
        this.maxSize = maxSize;
        for(T element: elements) {
            this.enqueue(element);
        }
    }

    public void enqueue(T element) {
        if (!isFull()) {
            linkedList.append(element);
        }
    }

    public T dequeue() throws NoSuchElementException{
        T result = peek();

        linkedList.removeFirst();

        return result;
    }

    public T peek() throws NoSuchElementException{
        if (!isEmpty()) {

            return linkedList.first();
        }

        throw new NoSuchElementException("Queue is empty");
    }


    public boolean isFull() {
        return maxSize >= 0 && size() == maxSize;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return linkedList.size();
    }
}

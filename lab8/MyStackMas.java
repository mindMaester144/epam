package lab8;

import java.util.EmptyStackException;
import java.util.Stack;

public class MyStackMas<T> {

    private T[] array ;
    private int stackHead;
    private boolean isEndless;

    public MyStackMas() {
        array = (T[]) new Object[10];
        stackHead = 0;
        isEndless = true;

    }

    public MyStackMas(T[] elements) {
        array = elements;
        stackHead = elements.length;
        isEndless = true;
    }

    public MyStackMas(int stackSize) {
        if(stackSize < 0) {
            throw new RuntimeException("Size can't be negative!");
        }

        array = (T[]) new Object[stackSize];
        stackHead = 0;
        isEndless = false;
    }

    public MyStackMas(T[] elements, int stackSize) {
        if(stackSize < 0) {
            throw new RuntimeException("Size can't be negative!");
        }

        array = (T[]) new Object[stackSize];
        stackHead = elements.length;
        isEndless = false;

        for (T element: elements) {
            this.push(element);
        }
    }

    public void push(T element) {
        if (!isFull()) {
            if (stackHead == array.length) {
                T[] newArray = (T[]) new Object[array.length * 2];

                System.arraycopy(array, 0, newArray, 0, stackHead);

                array = newArray;
            }

            array[stackHead] = element;
            stackHead++;
        }
    }

    public T pop() throws EmptyStackException{
        T result = peek();
        stackHead--;

        return result;
    }


    public T peek() throws EmptyStackException{
        if (!isEmpty()) {
            return array[stackHead - 1];
        }

        throw new EmptyStackException();
    }

    public int size() {
        return stackHead;
    }

    public boolean isFull() {
        return isEndless && stackHead == array.length;
    }

    public boolean isEmpty() {
        return stackHead == 0;
    }
}

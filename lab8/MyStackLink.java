package lab8;

import java.util.EmptyStackException;

public class MyStackLink<T> {
    private MyLinkedList<T> linkedList;
    private int maxSize;

    public MyStackLink() {
        linkedList = new MyLinkedList<T>();
        maxSize = -1;

    }

    public MyStackLink(T[] elements) {
        linkedList = new MyLinkedList<T>(elements);
        maxSize = -1;
    }

    public MyStackLink(int stackSize) {
        if(stackSize < 0) {
            throw new RuntimeException("Size can't be negative!");
        }

        linkedList = new MyLinkedList<T>();
        maxSize = stackSize;
    }

    public MyStackLink(T[] elements, int stackSize) {
        if(stackSize < 0) {
            throw new RuntimeException("Size can't be negative!");
        }

        maxSize = stackSize;
        for (int i = 0; i < maxSize; i++) {
            linkedList.put(elements[i]);
        }
    }

    public void push(T element) {
        if (!isFull()) {
            linkedList.put(element);
        }
    }

    public T pop() throws EmptyStackException{
        T result = peek();
        linkedList.removeFirst();

        return result;
    }


    public T peek() throws EmptyStackException {
        if (!isEmpty()) {
            return linkedList.last();
        }

        throw new EmptyStackException();
    }

    public int size() {
        return linkedList.size();
    }

    public boolean isFull() {
        return maxSize >= 0 && maxSize == size();
    }

    public boolean isEmpty() {
        return size() == 0;
    }
}

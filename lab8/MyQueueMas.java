package lab8;

import java.util.NoSuchElementException;

public class MyQueueMas<T> {
    private MyStackMas<T> inputStack;
    private MyStackMas<T> outputStack;
    private int maxSize;

    public MyQueueMas() {
        inputStack = new MyStackMas<>();
        outputStack = new MyStackMas<>();
        maxSize = -1;
    }

    public MyQueueMas(T[] elements) {
        inputStack = new MyStackMas<>(elements);
        outputStack = new MyStackMas<>();
        maxSize = -1;
    }

    public MyQueueMas(int maxSize) throws RuntimeException{
        if(maxSize < 0) {
            throw new RuntimeException("Size can't be negative!");
        }

        inputStack = new MyStackMas<>();
        outputStack = new MyStackMas<>();
        this.maxSize = maxSize;
    }

    public MyQueueMas(T[] elements, int maxSize) throws RuntimeException{
        if(maxSize < 0) {
            throw new RuntimeException("Size can't be negative!");
        }

        inputStack = new MyStackMas<>(elements, maxSize);
        outputStack = new MyStackMas<>();
        this.maxSize = maxSize;
    }

    public void enqueue(T element) {
        if (!isFull()) {
            inputStack.push(element);
        }
    }

    public T dequeue() throws NoSuchElementException{
        T result = peek();

        outputStack.pop();

        return result;
    }

    public T peek() throws NoSuchElementException{
        if (!isEmpty()) {
            if (outputStack.isEmpty()) {
                while (!inputStack.isEmpty()) {
                    outputStack.push(inputStack.pop());
                }
            }

            return outputStack.peek();
        }

        throw new NoSuchElementException("Queue is empty");
    }


    public boolean isFull() {
        return maxSize >= 0 && size() == maxSize;
    }

    public boolean isEmpty() {
        return size() > 0;
    }

    public int size() {
        return inputStack.size() + outputStack.size();
    }
}

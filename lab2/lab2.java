package lab2;

import javax.annotation.processing.SupportedSourceVersion;

public class lab2 {
    public static void main(String[] argv) {
        Task1.main(13);
        Task2.main(16, 13);
        Task3.main(16, 13);
        Task4.main(1234);
        Task5.main(12345);
        Task6.main(444222);
        Task7.main(1234567);
    }
}

class Task1 {
    public static final int CONVERT_MILLIGRAMS = 1000000;
    public static final int CONVERT_GRAMS = 1000;
    public static final double CONVERT_TONS = 1 / 1000.;

    public static void main(int kilos) {
        System.out.println("Task1:");

        double tons = kilosToTons(kilos);
        int grams = kilosToGrams(kilos);
        int milligrams = kiloToMilligrams(kilos);

        System.out.printf("Convert weight: %d kg;\n", kilos);
        System.out.printf("Tons: %f;\n", tons);
        System.out.printf("Gram: %d;\n", grams);
        System.out.printf("Milligram: %d.\n", milligrams);
    }

    public static int kiloToMilligrams(int kilos) {
        return kilos * CONVERT_MILLIGRAMS;
    }

    public static int kilosToGrams(int kilos) {
        return kilos * CONVERT_GRAMS;
    }

    public static double kilosToTons(int kilos) {
        return kilos * CONVERT_TONS;
    }
}

class Task2 {
    public static void main(int R1, int R2) {
        System.out.println("Task2:");

        System.out.printf("Ring(R1=%d, R2=%d) radius: %f.\n", R1, R2, findRingRadius(R1, R2));
    }

    public static double findRingRadius(int R1, int R2) {
        double circle1Area = getCircleArea(R1);
        double circle2Area = getCircleArea(R2);

        return Math.abs(circle1Area - circle2Area);
    }

    public static double getCircleArea(int radius) {
        return Math.PI * (radius * radius);
    }
}

class Task3 {
    public static void main(int a, int b) {
        System.out.println("Task3:");

        System.out.printf("Before: a=%d, b=%d;", a, b);

        a += b - (b = a);

        System.out.printf("After: a=%d, b=%d.", a, b);
    }

}

class Task4 {
    public static void main(int number) {
        System.out.println("Task4:");

        boolean result = checkNumber(number);

        System.out.printf("Number: %d, good: %b\n", number, result);
    }

    public static boolean checkNumber(int number) {
        return !(number % 10 <= (number /= 10) % 10 || number % 10 <= (number /= 10) % 10 || number % 10 <= (number / 10) % 10);
    }
}

class Task5 {
    public static void main(int number) {
        System.out.println("Task5:");

        int sumResult = findSum(number);
        int mulResult = findMul(number);

        System.out.printf("Number: %d, sum: %d, mul: %d.\n", number, sumResult, mulResult);
    }

    public static int findSum(int number) {
        return number % 10 + (number /= 10) % 10 + (number /= 10) % 10 + (number /= 10) % 10 + (number / 10) % 10;
    }

    public static int findMul(int number) {
        return number % 10 * ((number /= 10) % 10) * ((number /= 10) % 10) * ((number /= 10) % 10) * ((number / 10) % 10);
    }
}

class Task6 {
    public static void main(int number) {
        System.out.println("Task6:");

        double geometricMeanResult = findGeometricMean(number);
        double arithmeticMeanResult = findArithmeticMean(number);

        System.out.printf("Number: %d, geometric: %f, arithmetic: %f.\n", number, geometricMeanResult, arithmeticMeanResult);
    }

    public static double findGeometricMean(int number) {
        return Math.pow(number % 10 * ((number /= 10) % 10) * ((number /= 10) % 10) * ((number /= 10) % 10) * ((number /= 10) % 10) * ((number / 10) % 10), 1 / 6.);
    }

    public static double findArithmeticMean(int number) {
        return (number % 10 + (number /= 10) % 10 + (number /= 10) % 10 + (number /= 10) % 10 + (number /= 10) % 10 + (number / 10) % 10) / 6.0;
    }
}

class Task7 {
    public static void main(int number) {
        System.out.println("Task7:");

        int reversedNumber = reverseNumber(number);

        System.out.printf("Number: %d, reversed: %d.\n", number, reversedNumber);
    }

    public static int reverseNumber(int number) {
        return (number % 10) * 1000000 + (number /= 10) % 10 * 100000 + (number /= 10) % 10 * 10000 + (number /= 10) % 10 * 1000 + (number /= 10) % 10 * 100 + (number /= 10) % 10 * 10 + (number / 10) % 10;
    }

}
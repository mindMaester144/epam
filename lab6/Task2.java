package lab6;

public class Task2 {
    public static String findMaxFromLocalMaximum(int[][] mas) {
        int max_i = -1;
        int max_j = -1;

        for (int i = 1; i < mas.length - 1; i++) {
            for (int j = 1; j < mas[0].length - 1; j++) {
                if (mas[i - 1][j - 1] < mas[i][j]
                        && mas[i - 1][j] < mas[i][j]
                        && mas[i][j - 1] < mas[i][j]
                        && mas[i + 1][j + 1] < mas[i][j]
                        && mas[i + 1][j] < mas[i][j]
                        && mas[i][j + 1] < mas[i][j]
                        && mas[i + 1][j - 1] < mas[i][j]
                        && mas[i - 1][j + 1] < mas[i][j]) {
                     if (max_i == -1) {
                         max_i = i;
                         max_j = j;
                     }
                     else if (mas[max_i][max_j] < mas[i][j]) {
                         max_i = i;
                         max_j = j;
                     }
                }
            }

        }

        return max_i + " " + max_j;
    }
}

package lab6;

import java.util.Arrays;

public class Tests {
    public static void main(String[] argv) {
        testTask1();
        testTask2();
    }

    public static void testTask1() {
        System.out.println("\nTask1:");

        int[][] testMas = new int[][]{
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
        };

        System.out.println("Find extreme:");
        System.out.println(Task1.findExtreme(testMas));

        System.out.println("Find geometric and arithmetic mean:");
        System.out.println(Task1.findGeometricAndArithmeticMean(testMas));

        System.out.println("Find local minimum or maximum:");
        System.out.println(Task1.findLocalMinimumOrMaximum(testMas));

        System.out.println("Transposed matrix:");
        int[][] result = Task1.transposeMatrix(testMas);
        for (int[] line: result) {
            System.out.println(Arrays.toString(line));
        }
    }

    public static void testTask2() {
        System.out.println("\nTask2:");

        int[][] testMas = new int[][]{
                {1, 1, 1, 1, 1},
                {1, 2, 1, 5, 1},
                {1, 1, 1, 1, 1},
        };

        System.out.println("Find max local maximum:");
        System.out.println(Task2.findMaxFromLocalMaximum(testMas));

    }

}

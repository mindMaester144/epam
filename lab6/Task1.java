package lab6;

public class Task1 {

    public static String findExtreme(int[][] mas) {
        int max = mas[0][0];
        int min = mas[0][0];

        for (int[] line : mas) {
            for (int elem: line) {
                max = max < elem ? elem : max;
                min = min > elem ? elem : min;
            }
        }

        return max + " " + min;
    }

    public static String findGeometricAndArithmeticMean(int[][] mas) {
        double arithmeticMean = findArithmeticMean(mas);
        double geometricMean = findGeometricMean(mas);

        return arithmeticMean + " " + geometricMean;
    }

    private static double findGeometricMean(int[][] mas) {
        int mul = 1;

        for(int[] line : mas) {
            for (int elem : line) {
                mul *= elem;
            }
        }

        return Math.pow(mul, 1. / (mas.length + mas[0].length));
    }

    private static double findArithmeticMean(int[][] mas) {
        int sum = 0;

        for(int[] line : mas) {
            for (int elem : line) {
                sum += elem;
            }
        }

        return (double) sum / (mas.length + mas[0].length);
    }

    public static String findLocalMinimumOrMaximum(int[][] mas) {
        for (int i = 1; i < mas.length - 1; i++) {
            for (int j = 1; j < mas[0].length - 1; j++) {
                if ((mas[i - 1][j - 1] > mas[i][j]
                        && mas[i - 1][j] > mas[i][j]
                        && mas[i][j - 1] > mas[i][j]
                        && mas[i + 1][j + 1] > mas[i][j]
                        && mas[i + 1][j] > mas[i][j]
                        && mas[i][j + 1] > mas[i][j]
                        && mas[i + 1][j - 1] > mas[i][j]
                        && mas[i - 1][j + 1] > mas[i][j])
                        || (mas[i - 1][j - 1] < mas[i][j]
                        && mas[i - 1][j] < mas[i][j]
                        && mas[i][j - 1] < mas[i][j]
                        && mas[i + 1][j + 1] < mas[i][j]
                        && mas[i + 1][j] < mas[i][j]
                        && mas[i][j + 1] < mas[i][j]
                        && mas[i + 1][j - 1] < mas[i][j]
                        && mas[i - 1][j + 1] < mas[i][j])) {
                    return i + " " + j;
                }
            }

        }

        return "-1 -1";
    }

    public static int[][] transposeMatrix(int[][] mas) {
        int[][] transposedMatrix = new int[mas.length][mas[0].length];

        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas[0].length; j++) {
                transposedMatrix[j][i] = mas[i][j];
            }
        }

        return transposedMatrix;
    }
}

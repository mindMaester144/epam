package lab3;

import java.util.Random;

public class lab3 {
    public static void main(String[] argv) {
        Task1.main(2,2, 4, 2, 2, 6);
        Task2.main(0);
        Task3.main('a');
        Task4.main();
        Task5.main(1, 12, 1601);
    }
}


class Task1 {
    public static void main(int x1, int y1, int x2, int y2, int x3, int y3) {
        System.out.printf("\nTask1:\nPoints: (%d,%d), (%d,%d), (%d,%d).\n", x1, y1, x2, y2, x3, y3);

        double distance1_2 = getDistance(x1, y1, x2, y2);
        double distance1_3 = getDistance(x1, y1, x3, y3);
        double distance2_3 = getDistance(x2, y2, x3, y3);

        boolean isTriangle;
        boolean isRightTriangle = false;

        isTriangle = checkPoints(distance1_2, distance1_3, distance2_3);

        if (isTriangle) {
            double sqrDistance1_2 = getSqrDistance(x1, y1, x2, y2);
            double sqrDistance1_3 = getSqrDistance(x1, y1, x3, y3);
            double sqrDistance2_3 = getSqrDistance(x2, y2, x3, y3);

            isRightTriangle = checkTriangle(sqrDistance1_2, sqrDistance1_3, sqrDistance2_3);
        }

        System.out.printf("It is triangle: %b; Right: %b\n", isTriangle, isRightTriangle);
    }

    public static boolean checkPoints(double distance1_2, double distance1_3, double distance2_3) {
        return distance1_2 + distance1_3 > distance2_3 && distance1_2 + distance2_3 > distance1_3 && distance1_3 + distance2_3 > distance1_2;
    }

    public static boolean checkTriangle(double sqrDistance1_2, double sqrDistance1_3, double sqrDistance2_3) {
        return sqrDistance1_2 + sqrDistance1_3 == sqrDistance2_3 || sqrDistance1_2 + sqrDistance2_3 == sqrDistance1_3 || sqrDistance1_3 + sqrDistance2_3 == sqrDistance1_2;
    }

    public static double getDistance(int x1, int y1, int x2, int y2) {
        return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    public static double getSqrDistance(int x1, int y1, int x2, int y2) {
        return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
    }

}

class Task2 {
    public static final int BIRTH_HEADS_COUNT = 3;

    public static final int FIRST_PERIOD_HEADS = 3;
    public static final int SECOND_PERIOD_HEADS = 2;
    public static final int THIRD_PERIOD_HEADS = 1;

    public static final int FIRST_PERIOD_END = 200;
    public static final int SECOND_PERIOD_END = 300;

    public static void main(int N) {
        System.out.println("\nTask2:\n");

        int headCount = getHeadsCount(N);

        int eyesCount = headCount * 2;

        System.out.printf("Dragon(%d years) has: %d heads, %d eyes.", N, headCount, eyesCount);
    }

    public static int getHeadsCount(int N) {
        int result = 0;

        if (N >= 0) {
            result += BIRTH_HEADS_COUNT;

            if (N <= FIRST_PERIOD_END) {
                result += FIRST_PERIOD_HEADS * N;
            } else if (N <= SECOND_PERIOD_END){
                result += FIRST_PERIOD_HEADS * FIRST_PERIOD_END + SECOND_PERIOD_HEADS * (N - FIRST_PERIOD_END);
            } else {
                result += FIRST_PERIOD_HEADS * FIRST_PERIOD_END + SECOND_PERIOD_HEADS * (SECOND_PERIOD_END - FIRST_PERIOD_END) + THIRD_PERIOD_HEADS * (N - SECOND_PERIOD_END);
            }
        }

        return result;
    }
}

class Task3 {
    static char[] charsToFind = new char[] {'a', 'e', 'i', 'o', 'u'};

    public static void main(char c) {
        System.out.println("\nTask3:");

        c = Character.toLowerCase(c);

        boolean testResult1 = testChar1(c);
        boolean testResult2 = testChar2(c);
        boolean testResult3 = testChar3(c);
        boolean testResult4 = testChar4(c);


        System.out.printf("'%c' results: %b, %b, %b, %b\n", c, testResult1, testResult2, testResult3, testResult4);
    }

    public static boolean testChar1(char c) {
        return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'y';
    }

    public static boolean testChar2(char c) {
        switch (c) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
            case 'y':
                return true;
        }

        return false;
    }

    public static boolean testChar3(char c) {
        return (c + "").matches("^(?i:[aeiouy]).*");
    }

    public static boolean testChar4(char c) {
        return "aeiouy".contains(c + "");
    }
}

class Task4 {
    public static final int SMILES_COUNT = 4;

    public static final int VERY_NEGATIVE_SMILE_NUMBER = 0;
    public static final int NEGATIVE_SMILE_NUMBER = 1;
    public static final int POSITIVE_SMILE_NUMBER = 2;
    public static final int VERY_POSITIVE_SMILE_NUMBER = 3;

    public static final String VERY_NEGATIVE_SMILE = "XO";
    public static final String NEGATIVE_SMILE = ":(";
    public static final String POSITIVE_SMILE = ":)";
    public static final String VERY_POSITIVE_SMILE = ":D";

    public static final Random random = new Random();

    public static void main() {
        System.out.println("\nTask4:");

        int moodId = getMood();

        showMood(moodId);
    }

    public static int getMood() {
        return random.nextInt(SMILES_COUNT);
    }

    public static void showMood(int id) {
        String smileToPrint = "";

        switch (id) {
            case VERY_NEGATIVE_SMILE_NUMBER:
                smileToPrint = VERY_NEGATIVE_SMILE;
                break;
            case NEGATIVE_SMILE_NUMBER:
                smileToPrint = NEGATIVE_SMILE;
                break;
            case POSITIVE_SMILE_NUMBER:
                smileToPrint = POSITIVE_SMILE;
                break;
            case VERY_POSITIVE_SMILE_NUMBER:
                smileToPrint = VERY_POSITIVE_SMILE;
                break;

            default:
                return;
        }

        System.out.println(smileToPrint);
    }

}

class Task5 {
    public static final int JANUARY = 1;
    public static final int FEBRUARY = 2;
    public static final int MARCH = 3;
    public static final int APRIL = 4;
    public static final int MAY = 5;
    public static final int JUNE = 6;
    public static final int JULY = 7;
    public static final int AUGUST = 8;
    public static final int SEPTEMBER = 9;
    public static final int OCTOBER = 10;
    public static final int NOVEMBER = 11;
    public static final int DECEMBER = 12;

    public static final int FEBRUARY_LAST_DAY = 28;
    public static final int FEBRUARY_LAST_DAY_LEAP = 29;

    public static final int DAYS_IN_MONTH_WITH_31_DAYS = 31;
    public static final int DAYS_IN_MONTH_WITH_30_DAYS = 30;



    public static void main(int firstNumber, int secondNumber, int thirdNumber) {
        System.out.println("\nTask5:");

        String nextDateString = getNextDateAsString(firstNumber, secondNumber, thirdNumber);

        System.out.printf("Now: %d.%d.%d, next: %s", firstNumber, secondNumber, thirdNumber, nextDateString);


    }

    public static String getNextDateAsString(int day, int month, int year) {
        boolean isLeapYear = checkIfLeapYear(year);
        int nextDay = day;
        int nextMonth = month;
        int nextYear = year;
        int lastDay;
        boolean isLastMonth = false;


        switch (month) {
            case JANUARY:
            case MARCH:
            case MAY:
            case JULY:
            case AUGUST:
            case OCTOBER:
               lastDay = DAYS_IN_MONTH_WITH_31_DAYS;
                break;
            case JUNE:
            case SEPTEMBER:
            case NOVEMBER:
            case APRIL:
                lastDay = DAYS_IN_MONTH_WITH_30_DAYS;
                break;
            case FEBRUARY:
                lastDay = isLeapYear ? FEBRUARY_LAST_DAY_LEAP : FEBRUARY_LAST_DAY;
                break;
            case DECEMBER:
                isLastMonth = true;
                lastDay = DAYS_IN_MONTH_WITH_31_DAYS;
                break;

            default:
                return nextDay + "." + nextMonth + "." + nextYear;
        }

        if (day != lastDay) {
            nextDay++;
        }
        else {
            nextDay = 1;

            if (!isLastMonth) {
                nextMonth++;
            }
            else {
                nextMonth = 1;
                nextYear++;
            }
        }

        return nextDay + "." + nextMonth + "." + nextYear;
    }

    public static boolean checkIfLeapYear(int year) {
        return year % 4 == 0 && !(year % 100 == 0 && !(year % 400 == 0));
    }
}
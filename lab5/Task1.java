package lab5;

public class Task1 {
    public static String findExtreme(int[] mas) {
        int resultMin = findMin(mas);
        int resultMax = findMax(mas);

        return resultMin + " " + resultMax;
    }

    private static int findMax(int[] mas) {
        int result = mas[0];

        for(int elem: mas) {
            result = result < elem ? elem : result;
        }

        return result;
    }

    private static int findMin(int[] mas) {
        int result = mas[0];

        for(int elem: mas) {
            result = result > elem ? elem : result;
        }

        return result;
    }

    public static String findGeometricAndArithmeticMean(int[] mas) {
        double arithmeticMean = findArithmeticMean(mas);
        double geometricMean = findGeometricMean(mas);

        return arithmeticMean + " " + geometricMean;
    }

    private static double findGeometricMean(int[] mas) {
        int mul = 1;

        for(int elem: mas) {
            mul *= elem;
        }

        return Math.pow(mul, 1. / mas.length);
    }

    private static double findArithmeticMean(int[] mas) {
        int sum = 0;

        for(int elem: mas) {
            sum += elem;
        }

        return (double) sum / mas.length;
    }

    public static boolean checkIfSorted(int[] mas) {

        boolean isSorted = true;
        boolean isSortedReversed = true;

        for (int i = 1; i < mas.length; i++){
            if(mas[i] < mas[i - 1]) {
                isSorted = false;
            }

            if (mas[i] > mas[i - 1]){
                isSortedReversed = false;
            }

        }

        return isSorted || isSortedReversed;
    }

    public static int findLocalMinimumOrMaximum(int[] mas) {
        if(mas.length > 1 && mas[0] != mas[1]) {
            return 0;
        }

        for (int i = 1; i < mas.length - 1; i++) {
            if (mas[i - 1] < mas[i] && mas[i + 1] < mas[i]){
                return i;
            }
            if (mas[i - 1] > mas[i] && mas[i + 1] > mas[i]) {
                return i;
            }
        }

        if(mas.length > 1 && mas[mas.length - 2] != mas[mas.length - 1]) {
            return mas.length - 1;
        }

        return -1;
    }

    public static int[] reverseMas(int[] mas) {
        for (int i = 0; i < mas.length / 2; i++) {
            mas[i] += mas[mas.length - 1 - i] - (mas[mas.length - 1 - i] = mas[i]);
        }

        return mas;
    }

}

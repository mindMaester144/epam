package lab5;

public class Task2 {
    public static String findMaxAndSum(double[] mas) {
        if (mas.length != 0) {
            double max = mas[0];
            double sum = 0;
            boolean isNegativeNumbers = true;

            for (int i = mas.length - 1; i >= 0 ; i--) {
                max = max < mas[i] ? mas[i] : max;

                if (isNegativeNumbers) {
                    if (mas[i] <= 0) {
                        continue;
                    }

                    isNegativeNumbers = false;
                }

                sum += mas[i];
            }

            return max + " " + sum;
        }

        return "0 0";
    }
}

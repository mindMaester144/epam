package lab5;

import java.util.Arrays;

public class Tests {
    public static void main(String[] argv) {
        testTask1();
        testTask2();
    }

    public static void testTask1() {
        System.out.println("\nTask1:");

        int[] testMas = new int[]{1, 2, 3, 4, 5, 6};

        System.out.println("Find extreme:");
        System.out.println(Task1.findExtreme(testMas));

        System.out.println("Check if sorted:");
        System.out.println(Task1.checkIfSorted(testMas));

        System.out.println("Find geometric and arithmetic mean:");
        System.out.println(Task1.findGeometricAndArithmeticMean(testMas));

        System.out.println("Find local minimum or maximum:");
        System.out.println(Task1.findLocalMinimumOrMaximum(testMas));

        System.out.println("Reverse mas:");
        System.out.println(Arrays.toString(Task1.reverseMas(testMas)));
    }

    public static void testTask2() {
        System.out.println("\nTask2:");

        double[] testMas = {-1, 2, 1, -3, -22, };

        System.out.println("Max and sum:");
        System.out.println(Task2.findMaxAndSum(testMas));
    }
}
